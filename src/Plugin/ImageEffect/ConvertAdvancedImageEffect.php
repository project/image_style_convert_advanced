<?php

namespace Drupal\image_style_convert_advanced\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * Converts an image resource.
 *
 * @ImageEffect(
 *   id = "image_convert_advanced",
 *   label = @Translation("Convert (Advanced)"),
 *   description = @Translation("Convert specified image types to new image type.")
 * )
 */
class ConvertAdvancedImageEffect extends ConfigurableImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image) {
    $allowed_types = [];

    foreach ($this->configuration['file_types'] as $type) {
      $type = $image->getToolkit()->extensionToImageType($type);
      $allowed_types[$type] = $type;
    }
    if (isset($allowed_types[$image->getToolkit()->getType()])) {
      if (!$image->convert($this->configuration['extension'])) {
        $this->logger->error('Image convert failed using the %toolkit toolkit on %path (%mimetype)', ['%toolkit' => $image->getToolkitId(), '%path' => $image->getSource(), '%mimetype' => $image->getMimeType()]);
        return FALSE;
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeExtension($extension) {
    if (in_array($extension, $this->configuration['file_types'])) {
      return $this->configuration['extension'];
    }
    return $extension;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary = [
      '#markup' => implode(', ', array_map('mb_strtoupper', $this->configuration['file_types'])) . ' to ' .  mb_strtoupper($this->configuration['extension']),
    ];
    $summary += parent::getSummary();

    return $summary;
  }

  /**
   * Returns options array of supported image extensions.
   *
   * @return array
   *   Image extension options.
   */
  public function getOptions() {
    $extensions = \Drupal::service('image.toolkit.manager')->getDefaultToolkit()->getSupportedExtensions();

    return array_combine(
      $extensions,
      array_map('mb_strtoupper', $extensions)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'extension' => NULL,
      'file_types' => array_keys($this->getOptions()),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = $this->getOptions();

    $form['file_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('File Types to Convert'),
      '#options' => $options,
      '#default_value' => $this->configuration['file_types'],
      '#required' => TRUE,
    ];
    $form['extension'] = [
      '#type' => 'select',
      '#title' => t('Extension'),
      '#default_value' => $this->configuration['extension'],
      '#required' => TRUE,
      '#options' => $options,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['extension'] = $form_state->getValue('extension');
    $this->configuration['file_types'] = array_filter($form_state->getValue('file_types'));
  }

}
